class Human {

    void walk(){
      print("Humen can walk");
    }
    void run(){
      print("Humen can run");
    }
    void talk(){
      print("Humen can talk");
    }
    void read(){
      print("Humen can read");
    }
    void watch(){
      print("Humen can watch");
    }

}

class Women extends Human {
 void women() => print('women');

}
class Men extends Human {
 void men() => print('men');
}
class Childhood extends Human {
 void childhood() => print('childhood');
}
class Young extends Human {
 void young() => print('young');
}
class Adult extends Human {
 void adult() => print('adult');
}

void main(){
  
  var women = new Women();
  women.run();
  women.walk();
  women.talk();
  women.read();
  women.watch();
  women.women();
 
  print("--------------");

  var men = new Men();
  men.run();
  men.walk();
  men.talk();
  men.read();
  men.watch();
  men.men();
  
  print("--------------");

  var childhood = new Childhood();
  childhood.run();
  childhood.walk();
  childhood.talk();
  childhood.read();
  childhood.watch();
  childhood.childhood();

  print("--------------");

  var young = new Young();
  young.run();
  young.walk();
  young.talk();
  young.read();
  young.watch();
  young.young();

  print("--------------");

  var adult = new Adult();
  adult.run();
  adult.walk();
  adult.talk();
  adult.read();
  adult.watch();
  adult.adult();

}
